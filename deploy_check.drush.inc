<?php
/**
 * @file
 * Checks production environment conditions and best config automatically.
 */

/**
 * Implements hook_drush_command().
 */
function deploy_check_drush_command() {
  $items = array();

  $items['deploy-check'] = array(
    'callback' => 'deploy_check',
    'description' => dt('Checks production environment for required configuration and best practises.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'url' => dt('URL of the site'),
      'test-mail' => dt('Available test email address that you have access for'),
      'company' => dt('Domain name of the development company, ie for ex@example.com, it would be "example"'),
    ),
    'required-arguments' => TRUE,
  );

  return $items;
}

/**
 * Executes all the available checks before going to production.
 */
function deploy_check($url = NULL, $test_mail = NULL, $company = NULL) {
  $component_list = module_list();
  $component_list['deploy_check'] = 'deploy_check';
  $list = array();
  foreach ($component_list as $component) {
    $func = $component . '_deploy_check_list';
    if (function_exists($func)) {
      $list = array_merge($list, $func());
    }
  }
  $pass_count = 0;
  global $base_url;
  $base_url = $url;
  $params = array(
    'url' => $url,
    'test_mail' => $test_mail,
    'company' => $company,
  );
  foreach ($list as $check) {
    if ($check['callback']($params)) {
      drush_log($check['title'], 'ok');
      $pass_count++;
    }
    else {
      drush_set_error('deploy_check', $check['title']);
    }
  }
  $percentage = round(($pass_count / count($list)) * 100);
  if ($percentage > 0) {
    drush_log(round(($pass_count / count($list)) * 100) . " % ok", 'warning');
  }
  else {
    drush_log(dt('Good job, the site is ready to go live!'), 'ok');
  }
}

/**
 * Implements hook_deploy_check_list().
 */
function deploy_check_deploy_check_list() {
  return array(
    array(
      'title' => dt('Devel module is disabled'),
      'callback' => 'deploy_check_devel',
    ),
    array(
      'title' => dt('Migrate module is disabled'),
      'callback' => 'deploy_check_migrate',
    ),
    array(
      'title' => dt('Coder module is disabled'),
      'callback' => 'deploy_check_coder',
    ),
    array(
      'title' => dt('Database logging is enabled'),
      'callback' => 'deploy_check_log',
    ),
    array(
      'title' => dt('The site can send email'),
      'callback' => 'deploy_check_mail',
    ),
    array(
      'title' => dt('Cron is configured'),
      'callback' => 'deploy_check_cron',
    ),
    array(
      'title' => dt('Site is completely indexed.'),
      'callback' => 'deploy_check_search',
    ),
    array(
      'title' => dt('CSS & JS preprocessing is enabled.'),
      'callback' => 'deploy_check_js_css_preproc',
    ),
    array(
      'title' => dt('Source code is under Git, master branch is active, working copy is clean.'),
      'callback' => 'deploy_check_git',
    ),
    array(
      'title' => dt('Anonymous page cache is enabled.'),
      'callback' => 'deploy_check_anon_page_cache',
    ),
    array(
      'title' => dt('No account creation is enabled.'),
      'callback' => 'deploy_check_account_creation',
    ),
    array(
      'title' => dt('Google Analytics is configured.'),
      'callback' => 'deploy_check_google_analytics',
    ),
    array(
      'title' => dt('Site email is not from the development company.'),
      'callback' => 'deploy_check_site_email',
    ),
    array(
      'title' => dt('Site homepage is not awfully slow for (1 second for homepage as anon).'),
      'callback' => 'deploy_check_anon_homepage_perf',
    ),
    array(
      'title' => dt('Domain has MX record'),
      'callback' => 'deploy_check_domain_mx',
    ),
    array(
      'title' => dt('Favicon is replaced or not present'),
      'callback' => 'deploy_check_favicon',
    ),
    array(
      'title' => dt('PHP file upload configuration is sane'),
      'callback' => 'deploy_check_file_upload',
    ),
    array(
      'title' => dt('For PHP (v < 5.4), APC is available and has reasonable memory.'),
      'callback' => 'deploy_check_apc',
    ),
    array(
      'title' => dt('No errors at Status report page'),
      'callback' => 'deploy_check_status_page',
    ),
    array(
      'title' => dt('No development company-like email addresses in Rules configuration'),
      'callback' => 'deploy_check_rules_email',
    ),
  );
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks whether devel module is disabled.
 */
function deploy_check_devel() {
  return !module_exists('devel');
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks whether migrate module is disabled.
 */
function deploy_check_migrate() {
  return !module_exists('migrate');
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks whether coder module is disabled.
 */
function deploy_check_coder() {
  return !module_exists('coder');
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks whether database logging is enabled.
 */
function deploy_check_log() {
  return module_exists('dblog');
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks whether Google Analytics is configured.
 */
function deploy_check_google_analytics() {
  $config = variable_get('googleanalytics_account', '');
  return module_exists('google_analytics') && !empty($config);
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks whether page cache for anonymous is enabled.
 */
function deploy_check_anon_page_cache() {
  return variable_get('cache', FALSE);
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks whether account creation is disabled.
 */
function deploy_check_account_creation() {
  return variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL) == USER_REGISTER_ADMINISTRATORS_ONLY;
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks whether the site email is set to a proper value.
 */
function deploy_check_site_email($params) {
  return strstr(variable_get('site_mail', ''), $params['company']) === FALSE;
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks whether the site email is set to a proper value.
 */
function deploy_check_anon_homepage_perf($params) {
  $start = round(microtime(TRUE) * 1000);
  file_get_contents($params['url']);
  $loaded = round(microtime(TRUE) * 1000) - $start;
  if ($loaded > 1000) {
    drush_print(dt("The homepage was loaded in @time ms.", array('@time' => $loaded)));
    return FALSE;
  }
  return TRUE;
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks whether the domain has MX records.
 */
function deploy_check_domain_mx($params) {
  $parts = parse_url($params['url']);
  $records = dns_get_record($parts['host'], DNS_MX);
  return !empty($records);
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks whether CSS & JS aggregation is enabled..
 */
function deploy_check_js_css_preproc() {
  return variable_get('preprocess_css', FALSE) && variable_get('preprocess_js', FALSE);
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks whether the mail sending is available.
 */
function deploy_check_mail($params) {
  $target_address = $params['test_mail'];
  $params = array(
    'subject' => 'Test prod-check',
    'message' => time(),
  );
  $message = drupal_mail(
    'prodcheck',
    'prodcheck',
    $target_address,
    'en',
    $params
  );
  if (!$message['result']) {
    return FALSE;
  }
  return drush_confirm(dt('An email was sent to @addr, have you received it?', array('@addr' => $target_address)));
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Cron is configured.
 */
function deploy_check_cron() {
  $last = variable_get('cron_last', 0);
  return (time() - $last) < 1800;
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Git is used, on master, clean.
 */
function deploy_check_git() {
  ob_start();
  system('git status');
  $result = ob_get_clean();
  ob_end_clean();
  if (!(strstr($result, 'On branch master') && strstr($result, 'nothing to commit') && strstr($result, 'working directory clean'))) {
    drush_print(dt('Git status message:'));
    drush_print($result);
    return FALSE;
  }
  return TRUE;
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Search-related checks.
 */
function deploy_check_search() {
  if (module_exists('search_api')) {
    // Checks server connectivity.
    $result = db_query("SELECT id FROM {search_api_server} WHERE enabled=1");
    foreach ($result as $server) {
      $server = search_api_server_load($server->id);
      if (!$server->ping()) {
        drush_set_error('deploy_check', dt('One of the Search API servers cannot be reached'));
        return FALSE;
      }
    }

    // Checks index completeness.
    $result = db_query("SELECT id FROM {search_api_index} WHERE enabled=1");
    foreach ($result as $index) {
      $index = search_api_index_load($index->id);
      $status = search_api_index_status($index);
      if ($status['indexed'] != $status['total']) {
        drush_set_error('deploy_check', dt('One of the indeces are not complete'));
        return FALSE;
      }
    }
  }
  return TRUE;
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Check if not the default favicon is used.
 */
function deploy_check_favicon($params) {
  $parts = parse_url($params['url']);
  $result = drupal_http_request('http://www.google.com/s2/favicons?domain=' . $parts['host']);
  if (!empty($result->data)) {
    if (md5($result->data) == '2a03a11f7f9a4c1add052a256cef1b3e') {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Check if at least 20 MBytes large file uploads are possible.
 */
function deploy_check_file_upload($params) {
  $upload = _deploy_check_phpinfo_get('upload_max_filesize', $params['url']);
  $post = _deploy_check_phpinfo_get('post_max_size', $params['url']);
  $limit = 52428800;
  if ($upload < $limit || $post < $limit) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Callback for hook_deploy_check_list().
 *
 * For PHP 5.3 or earlier, checks if APC is enabled and has "enough" memory.
 */
function deploy_check_apc($params) {
  if (version_compare(PHP_VERSION, '5.4.0') == -1) {
    $shm = _deploy_check_phpinfo_get('apc.shm_size', $params['url']);
    $limit = 134217728;
    if ($shm < $limit) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks if there are no errors on status report page.
 */
function deploy_check_status_page() {
  module_load_include('inc', 'system', 'system.admin');
  return !(bool) system_status(TRUE);
}

/**
 * Callback for hook_deploy_check_list().
 *
 * Checks if Rules has potentially not updated mail address.
 */
function deploy_check_rules_email($params) {
  if (module_exists('rules')) {
    $config = rules_config_load_multiple(FALSE);
    foreach ($config as $conf) {
      $matches = array();
      $exp = $conf->export();
      preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $exp, $matches);
      if (count($matches[0])) {
        foreach ($matches[0] as $mail) {
          if (strstr($mail, $params['company']) !== FALSE) {
            drush_log(dt('Potentially not updated email at Rule: ' . $conf->label), 'warning');
            return FALSE;
          }
        }
      }
    }
  }
  return TRUE;
}

/**
 * Retrieves value from phpinfo() output.
 */
function _deploy_check_phpinfo_get($key, $url) {
  $phpinfo = &drupal_static(__FUNCTION__);

  // Fetches the phpinfo() through the webserver, it's important to not use
  // CLI version of the configuration file, it may totally differ!
  if (!isset($phpinfo)) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP script');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    $user_object = user_load(1);
    $link = url(user_pass_reset_url($user_object) . '/login', array('base_url' => $url));
    curl_setopt($ch, CURLOPT_URL, $link);
    $temp_name = drupal_tempnam('temporary://', 'file');
    $cookie_path = $temp_name;
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_path);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_path);
    curl_exec($ch);
    curl_setopt($ch, CURLOPT_URL, $url . '/admin/reports/status/php');
    $phpinfo = curl_exec($ch);
  }

  $lines = explode("\n", $phpinfo);
  $line = '';
  foreach ($lines as $line) {
    if (strstr($line, $key) !== FALSE) {
      break;
    }
  }
  $matches = array();
  preg_match('/([0-9]+[a-zA-Z]*)/', $line, $matches);
  $val = trim($matches[0]);
  $last = strtolower($val[strlen($val) - 1]);
  switch ($last) {
    // The 'G' modifier is available since PHP 5.1.0
    // there's no break intentionally.
    case 'g':
      $val *= 1024;
    case 'm':
      $val *= 1024;
    case 'k':
      $val *= 1024;
  }

  return (int) $val;
}
